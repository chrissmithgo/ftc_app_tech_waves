/* Tech Waves - Wavebot Version 1 - with Just the Sweeper */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import org.firstinspires.ftc.teamcode.HardwareShooter_only;



/**
 * All device access is managed through the HardwareWaveBot class.
 * The code is structured as a LinearOpMode
 *
 * This particular OpMode executes a POV Game style Teleop for a PushBot like robot ( 2 Power wheels )
 * In this mode the left stick moves the robot FWD and back, the Right stick turns left and right.
 */

@TeleOp(name="Wavebot: Shooter Only", group="Pushbot")
@Disabled
public class WaveBot_TeleopPOV_Linear_shooter extends LinearOpMode {

    /* Declare OpMode members. */
    HardwareShooter_only robot = new HardwareShooter_only();

    private boolean start;

    @Override
    public void runOpMode() {
       //  double left;
       // double right;
        double max;

        /* Initialize the hardware variables.
         * The init() method of the hardware class does all the work here
         */
        robot.init(hardwareMap);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Say", "Hello Driver");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
                // Use gamepad to start Pitch Motors
                if (gamepad1.x) {
                    robot.pitchMotorR.setPower(robot.PITCH_POWER_RIGHT);
                    robot.pitchMotorL.setPower(robot.PITCH_POWER_LEFT);
                }
                    else {
                    robot.pitchMotorR.setPower(0.0);
                    robot.pitchMotorL.setPower(0.0);
                }

            // Send telemetry message to signify robot running;
            //telemetry.addData("claw",  "Offset = %.2f", clawOffset);
            //telemetry.addData("left",  "%.2f", left);
            //telemetry.addData("right", "%.2f", right);
            telemetry.addData("Pitch Right ", "%.2f", robot.pitchMotorR.getPower());
            telemetry.addData("Pitch Left ", "%.2f", robot.pitchMotorL.getPower());
            // start = gamepad2.start;
            // telemetry.addData("gamepad", "%.2f", start);
            telemetry.update();

            // Pause for metronome tick.  40 mS each cycle = update 25 times a second.
            robot.waitForTick(40);
        }
    }
}
