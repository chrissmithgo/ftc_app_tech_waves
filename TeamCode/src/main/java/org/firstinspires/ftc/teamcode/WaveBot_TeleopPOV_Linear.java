/* Tech Waves - Wavebot Version 4 - with Sweeper, Shooter, Beacon */
/* 2017/02/05 Code Comments and Cleanup */
/* 2017/02/06 Update techwaves bitbucket repository */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.Range;


/**
 * All device access is managed through the HardwareWaveBot class.
 * The code is structured as a LinearOpMode
 *
 * This particular OpMode executes a POV Game style Teleop for a PushBot like robot ( 2 Power wheels )
 * In this mode the left stick moves the robot FWD and back, the Right stick turns left and right.
 * It raises and lowers the claw using the Gampad Y and A buttons respectively.
 * It also opens and closes the claws slowly using the left and right Bumper buttons.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="Wavebot: Teleop POV", group="Pushbot")
//@Disabled
public class WaveBot_TeleopPOV_Linear extends LinearOpMode {

    org.firstinspires.ftc.teamcode.HardwareWaveBot robot = new HardwareWaveBot();

    double          pitcherOffset      = 0;
    private boolean pitch_running = false;
    double beaconPosition = 0.0;

    @Override
    public void runOpMode() {
        double left;
        double right;
        double max;


        /* Initialize the hardware variables.
         * The init() method of the hardware class does all the work here
         */
        robot.init(hardwareMap);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Say", "Hello Driver");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // GAMEPAD   1 -- Driving


            // Run wheels in POV mode (note: The joystick goes negative when pushed forwards, so negate it)
            // In this mode the Left stick moves the robot fwd and back, the Right stick turns left and right.
            left  = -gamepad1.left_stick_y + gamepad1.right_stick_x;
            right = -gamepad1.left_stick_y - gamepad1.right_stick_x;

            // Normalize the values so neither exceed +/- 1.0
            max = Math.max(Math.abs(left), Math.abs(right));
            if (max > 1.0)
            {
                left /= max;
                right /= max;
            }

            robot.leftMotor.setPower(left);
            robot.rightMotor.setPower(right);

                // GAMEPAD  2 -- Use gamepad buttons to move arm up (Y) and down (A)
                // Use gamepad buttons to move Sweeper in (Y) and out (A) spins.
                if (gamepad2.a)
                    robot.sweepMotor.setPower(robot.SWEEP_IN_POWER);
                else if (gamepad2.y)
                    robot.sweepMotor.setPower(robot.SWEEP_OUT_POWER);
                else
                    robot.sweepMotor.setPower(0.0);

                // Use gamepad to start Pitch Motors
                if (gamepad2.x) {
                    robot.pitchMotorR.setPower(0.0);
                    robot.pitchMotorL.setPower(0.0);
                    pitch_running = false;
                    }
                else if (gamepad2.b ) {           //&& pitch_running = false
                    robot.pitchMotorR.setPower(robot.PITCH_POWER_RIGHT);
                    robot.pitchMotorL.setPower(robot.PITCH_POWER_LEFT);
                    pitch_running = true;
                }

                // ALT-- Shooter Style - Use gamepad bumper trigger for pitcher servo control
                if (gamepad2.right_bumper)
                    pitcherOffset = 0.30;  // Servo in UP position
                else
                    pitcherOffset = 0.75;  // Servo in DOWN position

                robot.pitcherServo.setPosition(pitcherOffset);

                // Use gamepad bumper left
                if (gamepad2.left_bumper)
                        beaconPosition = 1.0;  // Servo in OUT position
                    else
                    beaconPosition = 0.00;  // Servo in IN / RETRACTED position

                robot.beaconServo.setPosition(beaconPosition);


            // Send telemetry message to signify robot running;
            // telemetry.addData("drive left", robot.leftMotor.getPower()  );
            //telemetry.addData("drive right", robot.rightMotor.getPower());
            telemetry.addData("sweep", "%.2f", robot.sweepMotor.getPower());

            telemetry.addData("pitch right", "%.2f", robot.pitchMotorR.getPower());
            telemetry.addData("pitch left ", "%.2f", robot.pitchMotorR.getPower());
            telemetry.addData("pitcher Offset",  pitcherOffset);
            telemetry.addData("pitcher servo position", robot.pitcherServo.getPosition() );
            telemetry.addData("beaconPosition",  beaconPosition);
            telemetry.addData("beacon servo position", robot.beaconServo.getPosition() );
            telemetry.update();

            // Pause for metronome tick.  40 mS each cycle = update 25 times a second.
            robot.waitForTick(40);
        }
    }
}
