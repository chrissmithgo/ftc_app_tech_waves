/*
WaveBot Auto Program

SCORE - KNOCK BALL OFF STAND - PARK ON CORNER

2017/02/06 Refine movements...
*/
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;


@Autonomous(name="WaveBot: Auto Red (Shoot Ball Park)", group="Pushbot")

public class WaveBot_Auto_Red_Shoot_Ball_Park extends LinearOpMode {

    /* Declare OpMode members. */
    HardwareWaveBot robot = new HardwareWaveBot();
    private ElapsedTime     runtime = new ElapsedTime();


    static final double     FORWARD_SPEED = 1.0;
    static final double     TURN_SPEED    = 0.5;

    @Override
    public void runOpMode() {

        /*
         * Initialize the drive system variables.
         * The init() method of the hardware class does all the work here
         */
        robot.init(hardwareMap);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Step through each leg of the path, ensuring that the Auto mode has not been stopped along the way

//PAUSE AT START
        while (opModeIsActive() && (runtime.seconds() < 3)) {
            telemetry.addData("Path", "PAUSE AT START: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // Drive Backward for 1.2 seconds / 6'  ( READY TO SHOOT)
        robot.leftMotor.setPower(-1);
        robot.rightMotor.setPower(-1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1.2)) {
            telemetry.addData("Path", "READY TO SHOOT: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);

        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 2)) {
            telemetry.addData("Path", "STOP: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        //Turn Left for .35 seconds / 45 degrees  ( Turn toward center)
        robot.leftMotor.setPower(0.5);
        robot.rightMotor.setPower(-0.5);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .35)) {
            telemetry.addData("Path", "TURN TO CORNER: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);

        // START SHOOTER
        robot.pitchMotorR.setPower(robot.PITCH_POWER_RIGHT);
        robot.pitchMotorL.setPower(robot.PITCH_POWER_LEFT);

        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "START SHOOTER: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // FIRE BALL 1
        robot.pitcherServo.setPosition(0.3);  // Servo in UP position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "FIRE BALL 1: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        robot.pitcherServo.setPosition(0.75);  // Servo in DOWN position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 2)) {
            telemetry.addData("Path", "FIRE BALL 1 DONE: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // FIRE BALL 2
        robot.pitcherServo.setPosition(0.3);  // Servo in UP position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "FIRE BALL 2: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        robot.pitcherServo.setPosition(0.75);  // Servo in DOWN position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 2)) {
            telemetry.addData("Path", "FIRE BALL 2 DONE: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        //SHAKE THE BALLS
            // side to side turn
            robot.leftMotor.setPower(0.5);
            robot.rightMotor.setPower(-0.5);
            runtime.reset();
            while (opModeIsActive() && (runtime.seconds() < .2)) {
                telemetry.addData("Path", "left: %2.5f S Elapsed", runtime.seconds());
                telemetry.update();
            }
            // STOP
            robot.leftMotor.setPower(0);
            robot.rightMotor.setPower(0);
            runtime.reset();
            while (opModeIsActive() && (runtime.seconds() < .2)) {
                telemetry.addData("Path", "stop: %2.5f S Elapsed", runtime.seconds());
                telemetry.update();
            }
            robot.leftMotor.setPower(-0.5);
            robot.rightMotor.setPower(0.5);
            runtime.reset();
            while (opModeIsActive() && (runtime.seconds() < .2)) {
                telemetry.addData("Path", "right: %2.5f S Elapsed", runtime.seconds());
                telemetry.update();
            }
            // STOP
            robot.leftMotor.setPower(0);
            robot.rightMotor.setPower(0);

        // FIRE AGAIN JUST IN CASE OF STUCK BALL
        robot.pitcherServo.setPosition(0.3);  // Servo in UP position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "FIRE BALL 3 : %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        robot.pitcherServo.setPosition(0.75);  // Servo in DOWN position
        // WAIT
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "FIRE BALL 3 DONE: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP SHOOTER
        robot.pitchMotorR.setPower(0);
        robot.pitchMotorL.setPower(0);

/*
        //Turn left for .3 seconds / 45 degrees  ( Turn toward cap ball)
        robot.leftMotor.setPower(-0.5);
        robot.rightMotor.setPower(0.5);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .3)) {
            telemetry.addData("Path", "TURN TO CORNER: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);
*/

        // Drive backward for 1.5 seconds / 2'  ( Knock CAP BALL off)
        robot.leftMotor.setPower(-1);
        robot.rightMotor.setPower(-1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1.8)) {
            telemetry.addData("Path", "CAP BALL: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 1)) {
            telemetry.addData("Path", "AT CENTER: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        //SHAKE THE ROBOT
        // side to side turn
        robot.leftMotor.setPower(1);
        robot.rightMotor.setPower(-1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .2)) {
            telemetry.addData("Path", "left: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .2)) {
            telemetry.addData("Path", "stop: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        robot.leftMotor.setPower(-1);
        robot.rightMotor.setPower(1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .2)) {
            telemetry.addData("Path", "right: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);

        // Drive forward for x seconds / 1.5'  ( GO Home)
        robot.leftMotor.setPower(0.9);
        robot.rightMotor.setPower(1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .75)) {
            telemetry.addData("Path", "GO HOME: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        //Turn Left for .8 seconds / 120 degrees  ( Turn toward corner)
        robot.leftMotor.setPower(1);
        robot.rightMotor.setPower(-1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < .8)) {
            telemetry.addData("Path", "TURN TO CORNER: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }
        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);





        // Drive forward for x seconds / 6'  ( Park on corner)
        robot.leftMotor.setPower(0.9);
        robot.rightMotor.setPower(1);
        runtime.reset();
        while (opModeIsActive() && (runtime.seconds() < 5)) {
            telemetry.addData("Path", "GO HOME: %2.5f S Elapsed", runtime.seconds());
            telemetry.update();
        }

        // STOP
        robot.leftMotor.setPower(0);
        robot.rightMotor.setPower(0);



        telemetry.addData("Path", "Complete");
        telemetry.update();
        sleep(1000);
    }
}
