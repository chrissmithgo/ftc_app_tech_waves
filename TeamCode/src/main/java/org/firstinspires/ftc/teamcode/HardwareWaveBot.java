package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * This is NOT an opmode.
 *
 * This class can be used to define all the specific hardware for a single robot.
 * In this case that robot is a Wavebot
 */
public class HardwareWaveBot
{
    /* Public OpMode members. */
    public DcMotor leftMotor   = null;
    public DcMotor rightMotor  = null;
    public DcMotor sweepMotor    = null;
    public DcMotor pitchMotorR    = null;
    public DcMotor pitchMotorL    = null;
    public Servo   pitcherServo    = null;
    public Servo   beaconServo    = null;

    public static final double PITCH_POWER_RIGHT =  1.0 ;
    public static final double PITCH_POWER_LEFT  = -1.0 ;
    public static final double SWEEP_IN_POWER    =  0.70 ;
    public static final double SWEEP_OUT_POWER   = -1.0 ;
    public static final double START_SERVO       =  0.75 ;

    /* local OpMode members. */
    HardwareMap hwMap           =  null;
    private ElapsedTime period  = new ElapsedTime();

    /* Constructor */
    public HardwareWaveBot(){

    }

    /* Initialize standard Hardware interfaces */
    public void init(HardwareMap ahwMap) {
        // Save reference to Hardware map
        hwMap = ahwMap;

        // Define and Initialize Drive Motors
        leftMotor   = hwMap.dcMotor.get("left_drive");
        rightMotor  = hwMap.dcMotor.get("right_drive");
        sweepMotor =  hwMap.dcMotor.get("sweep_motor");
        leftMotor.setDirection(DcMotor.Direction.FORWARD); // Set to REVERSE if using AndyMark motors
        rightMotor.setDirection(DcMotor.Direction.REVERSE);// Set to FORWARD if using AndyMark motors

        // Set motors to zero power
        leftMotor.setPower(0);
        rightMotor.setPower(0);
        sweepMotor.setPower(0);

        // Set motors to run without encoders.
        // May want to use RUN_USING_ENCODERS if encoders are installed.
        leftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        sweepMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);


        // Define and Initialize Pitch Motors
        pitchMotorR    = hwMap.dcMotor.get("pitch_right");
        pitchMotorL =  hwMap.dcMotor.get("pitch_left");
        // Set motors to zero power
        pitchMotorR.setPower(0);
        pitchMotorL.setPower(0);
        // Set motors to run without encoders
        // May want to use RUN_USING_ENCODERS if encoders are installed.
        pitchMotorR.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        pitchMotorL.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // Define and initialize ALL installed servos.
        pitcherServo = hwMap.servo.get("pitch_servo");
        pitcherServo.setPosition(START_SERVO);
        beaconServo = hwMap.servo.get("beacon_servo");
        beaconServo.setPosition(0.0);

    }

    /***
     *
     * waitForTick implements a periodic delay. However, this acts like a metronome with a regular
     * periodic tick.  This is used to compensate for varying processing times for each cycle.
     * The function looks at the elapsed cycle time, and sleeps for the remaining time interval.
     *
     * @param periodMs  Length of wait cycle in mSec.
     */
    public void waitForTick(long periodMs) {

        long  remaining = periodMs - (long)period.milliseconds();

        // sleep for the remaining portion of the regular cycle period.
        if (remaining > 0) {
            try {
                Thread.sleep(remaining);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        // Reset the cycle clock for the next pass.
        period.reset();
    }
}

